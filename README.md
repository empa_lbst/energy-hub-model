# Energy hub model for the design, sizing and optimal operation of a building's energy system #

This repository contains the model file for an energy hub model that performs the optimal selection and sizing of the technologies to be installed for the energy system of a single building or an aggregation of loads of multiple buildings.

### How do I use the files included in the repository? ###

In order to use the model that is included in this repository, the user must initially have AIMMS installed on his computer. Then the following steps are required to import AIMMS.

* Start AIMMS and create a new project
* Using the menu the user must go to File > New > Model and select the .ams file from this repository.

### What can I do with the model? ###

In the current version electrical and heating loads are covered. The user initially must provide the model with hourly values for the electrical and thermal demands, as well as for the solar radiation profiles.

The model can be run with a full-year configuration (8760h) or using typical days. The value of the time steps need to be changed accordingly.

Two objectives are considered in the current version of the model, namely cost and carbon emissions minimisation. The user can select which objective to minimise and run the procedures *Cost_Minimization* and *Carbon_Minimization*, respectively.

### Where can I find more information? ###

The model description as well as the description of the results of a case study can be found in the following publication:

Mavromatidis, G., Evins, R., Orehounig, K., Dorer, V., Carmeliet, J. (2014) Multi-objective optimization to simultaneously address energy hub layout, sizing and scheduling using a linear formulation. In: 4th International Conference on Engineering Optimization (ENGOPT), September 8-11, 2014, Lisbon, Portugal,  603-608, CRC Press.